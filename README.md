The directed graph consists
of nodes and directed edges depicting probabilistic transitions between nodes. The
graph is entered in the node START and exited in the node STOP. Please provide
the value of 'counter' in each node.


![Graph](https://bytebucket.org/luis_palomero/ebi_task3/raw/f0228023b781789a189a0e615945de476e27064b/assets/graph.png)