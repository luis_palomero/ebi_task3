package com.luis.ebitests.graph;

public interface IRandomGenerator {
	public double generate();
}
