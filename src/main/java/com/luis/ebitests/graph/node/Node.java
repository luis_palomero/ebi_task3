package com.luis.ebitests.graph.node;

public class Node {
	
	public static final boolean END_NODE = true;
	public static final int UNDEFINED_COUNTER = -1;
	
	private int id;
	private String name;
	private double counter;
	private Link[] links;
	private boolean endNode;
	
	public Node(int id, String name, double counter, boolean endNode, Link[] links){
		this.id = id;
		this.name = name;
		this.counter = counter;
		this.endNode = endNode;
		this.links = links;
		this.checkProbability();
		
	}
	
	private void checkProbability() throws IllegalStateException{
		
		if(this.endNode == Node.END_NODE){
			return;
		}
		
		float sumProbability = 0;
		try{			
			for(int i = 0; i < links.length; i++){
				sumProbability+=links[i].getProbability();
			}
		} catch(NullPointerException e){
			//With empty array we must throw invalid probability
		}
		if(sumProbability != 1){
			throw new IllegalStateException("Invalid probability");
		}
	}
	
	public int getId(){
		return this.id;
	}
	
	public String getName(){
		return this.name;
	}
	
	public void setCounter(double newCounter){
		this.counter = newCounter;
	}
	
	public double getCounter() {
		return this.counter;
	}
	
	public Link[] getLinks(){
		return this.links;
	}
	
	public boolean isEndNode(){
		return this.endNode;
	}
	
	public int getIdNext(double probability){
		double accumulated = 0;
		int i = 0;
		for(; i < this.getLinks().length; i++){
			accumulated += this.getLinks()[i].getProbability();
			if(probability  <= accumulated) {
				break;
			}
		}
		return i;
	}
	
	public String toString(){
		double currentValue = this.getCounter();
		String strValue = (currentValue != Node.UNDEFINED_COUNTER)? String.valueOf(currentValue):"?";
		return "[".concat(this.getName()).concat("{").concat(strValue).concat("}]");
	}
}
