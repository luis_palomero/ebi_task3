package com.luis.ebitests.graph.node;

public class Link {
	private int id;
	private double probability;
	
	public Link(int id, double probability){
		this.id = id;
		this.probability = probability;
	}
	
	public int getId(){
		return this.id;
	}
	
	public double getProbability(){
		return this.probability;
	}

	
}
