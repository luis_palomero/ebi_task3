package com.luis.ebitests.graph.node;

public class RuleQuitOne implements IRule {
	public double applyRule(Node node) {
		return node.getCounter() - 1;
	}
}
