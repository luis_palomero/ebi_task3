package com.luis.ebitests.graph.node;

public interface IRule {
	public double applyRule(Node node);
}
