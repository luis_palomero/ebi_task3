package com.luis.ebitests.graph.node;

public class EndOrNoCounterStopCriteria implements IStopCriteria {
	public boolean checkStop(Node node) {
		return (node.isEndNode() || node.getCounter() == 0);
	}
}
