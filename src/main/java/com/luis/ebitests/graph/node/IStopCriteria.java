package com.luis.ebitests.graph.node;

public interface IStopCriteria {
	boolean checkStop(Node node);
}
