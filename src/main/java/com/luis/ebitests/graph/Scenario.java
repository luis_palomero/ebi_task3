package com.luis.ebitests.graph;

import com.luis.ebitests.graph.node.IRule;
import com.luis.ebitests.graph.node.IStopCriteria;
import com.luis.ebitests.graph.node.Node;

public class Scenario {
	private Node[] list;
	private int currentPosition;
	private IRandomGenerator generator;
	private IStopCriteria stopCriterum;
	private IRule nextRule;

	public Scenario(Node[] list, int startPosition, IRandomGenerator generator, IStopCriteria stopCriterium,
			IRule rule) {
		this.list = list;
		this.currentPosition = startPosition;
		this.generator = generator;
		this.stopCriterum = stopCriterium;
		this.nextRule = rule;
	}

	public Node[] getNodes() {
		return this.list;
	}

	public Node getCurrentNode() {
		return this.list[this.currentPosition];
	}

	public void setCurrentPosition(int pos) {
		this.currentPosition = pos;
	}

	public int getNext() {
		double generatedValue = this.generator.generate();
		int nextPosition = this.getCurrentNode().getIdNext(generatedValue);
		return nextPosition;

	}

	public void advance() {
		int next = this.getNext();
		double newCounter = this.nextRule.applyRule(this.getCurrentNode());
		this.setCurrentPosition(this.getCurrentNode().getLinks()[next].getId());
		this.getCurrentNode().setCounter(newCounter);
	}

	public boolean hasFinished() {
		return this.stopCriterum.checkStop(this.getCurrentNode());
	}

	public String toString() {
		StringBuilder output = new StringBuilder("{");
		for (int i = 0; i < this.getNodes().length; i++) {
			output.append(this.getNodes()[i].toString()).append(",");
		}
		output.replace(output.length() - 1, output.length(), "}");
		return output.toString();
	}

	public void run() {
		while (!this.hasFinished()) {
			this.advance();
		}
	}

}
