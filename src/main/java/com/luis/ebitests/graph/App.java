package com.luis.ebitests.graph;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.luis.ebitests.graph.node.EndOrNoCounterStopCriteria;
import com.luis.ebitests.graph.node.IRule;
import com.luis.ebitests.graph.node.IStopCriteria;
import com.luis.ebitests.graph.node.Link;
import com.luis.ebitests.graph.node.Node;
import com.luis.ebitests.graph.node.RuleQuitOne;

/**
 * Executes EBI third tests scenario an arbitrary number of iterations and store
 * the results at an ouptut file. This result is the aggregation of individual
 * execution, this file is unordered
 * 
 * For ordering it could be useful use: sort -t "," -k 6 -n fileName
 * 
 * Usage java java -jar programName iterations fileName
 *
 */
public class App {
	private static final int START_COUNTER = 10;

	public static void main(String[] args) throws IOException {
		int iterations;
		String filename;
		try {
			iterations = Integer.valueOf(args[0]);
			filename = args[1];
		} catch (Exception e) {
			System.err.println("Invalid params, correct usage is \"java -jar programName iterations fileName\"");
			return;
		}

		System.out.println("Starting scenario running");

		IStopCriteria stop = new EndOrNoCounterStopCriteria();
		IRule ruleNext = new RuleQuitOne();
		IRandomGenerator generator = new RandomUniform();

		Node[] nodes = buildNodeList();

		FileWriter file = new FileWriter(filename);

		HashMap<String, Integer> results = new HashMap<String, Integer>();

		for (int i = 0; i < iterations; i++) {
			Scenario scenario = new Scenario(nodes, 0, generator, stop, ruleNext);
			scenario.run();
			String strScenario = scenario.toString();
			if (results.containsKey(strScenario) == true) {
				results.put(strScenario, results.get(strScenario) + 1);
			} else {
				results.put(strScenario, 1);
			}
		}
		printResults(results, file, iterations);
		file.close();

		System.out.println("File " + filename + " generated successfully");

		System.out.println("Bash command 'sort -t \",\" -k 6 -n " + filename + "' will sort the file");
	}

	public static void printResults(HashMap<String, Integer> results, FileWriter file, int iterations)
			throws IOException {
		Iterator it = results.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry) it.next();
			int value = (Integer) pair.getValue();
			file.write(pair.getKey() + " , " + value + " , " + (double) value / (double) iterations * 100 + "%");
			file.write("\r\n");
			it.remove(); // avoids a ConcurrentModificationException
		}
	}

	public static Node[] buildNodeList() {
		Link[] nodeStartLinks = { new Link(1, 1) };
		Node nodeStart = new Node(0, "START", START_COUNTER, !Node.END_NODE, nodeStartLinks);

		Link[] node1Links = { new Link(2, 0.8), new Link(3, 0.2) };
		Node node1 = new Node(1, "Node1", Node.UNDEFINED_COUNTER, !Node.END_NODE, node1Links);

		Link[] node2Links = { new Link(1, 0.25), new Link(3, 0.75) };
		Node node2 = new Node(2, "Node2", Node.UNDEFINED_COUNTER, !Node.END_NODE, node2Links);

		Link[] node3Links = { new Link(1, 0.875), new Link(4, 0.125) };
		Node node3 = new Node(3, "Node3", Node.UNDEFINED_COUNTER, !Node.END_NODE, node3Links);

		Link[] nodeEndLinks = new Link[0];
		Node nodeEnd = new Node(4, "END", Node.UNDEFINED_COUNTER, Node.END_NODE, nodeEndLinks);

		Node[] nodes = { nodeStart, node1, node2, node3, nodeEnd };

		return nodes;
	}

}
