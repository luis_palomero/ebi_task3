package com.luis.ebitests.graph;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.luis.ebitests.graph.node.EndOrNoCounterStopCriteria;
import com.luis.ebitests.graph.node.IRule;
import com.luis.ebitests.graph.node.IStopCriteria;
import com.luis.ebitests.graph.node.Link;
import com.luis.ebitests.graph.node.Node;
import com.luis.ebitests.graph.node.RuleQuitOne;

public class ScenarioTest {

	private static final double DELTA = 1e-15;
	private static int COUNTER_START_VALUE = 10;

	private IStopCriteria stop;
	private IRule ruleNext;
	private IRandomGenerator generator;
	private IRandomGenerator mockGenerator;

	@Before
	public void setUp() {
		stop = new EndOrNoCounterStopCriteria();
		ruleNext = new RuleQuitOne();
		generator = new RandomUniform();
		mockGenerator = mock(RandomUniform.class);
	}

	@Test(expected = IllegalStateException.class)
	public void shouldLaunchExceptionWithNullNodeList() {
		// Arrange
		Link[] node1Links = null;
		Link[] node2Links = {};
		Node[] nodes = { new Node(1, "Node1", COUNTER_START_VALUE, !Node.END_NODE, node1Links),
				new Node(2, "Node2", 0, Node.END_NODE, node2Links),

		};

		Scenario scenario = new Scenario(nodes, 0, generator, stop, ruleNext);
	}

	@Test
	public void shouldSetFinishedStatusAtEndNode() {
		// Arrange
		Node[] nodes = { new Node(0, "Node1", COUNTER_START_VALUE, Node.END_NODE, null) };
		Scenario scenario = new Scenario(nodes, 0, generator, stop, ruleNext);

		// Act
		boolean hasFinished = scenario.hasFinished();

		// Assert
		assertTrue(hasFinished);
	}

	@Test
	public void shouldChangeCurrentNodeAfterJump() {
		// Arrange
		Link[] node1Links = { new Link(1, 1) };
		Link[] node2Links = {};
		Node[] nodes = { new Node(0, "Start", COUNTER_START_VALUE, !Node.END_NODE, node1Links),
				new Node(1, "End", 0, Node.END_NODE, node2Links),

		};

		Scenario scenario = new Scenario(nodes, 0, generator, stop, ruleNext);
		// Act
		scenario.advance();
		// Assert
		assertEquals(nodes[1].getId(), scenario.getCurrentNode().getId());
		assertEquals(COUNTER_START_VALUE - 1, scenario.getNodes()[1].getCounter(), DELTA);
		assertEquals("{[Start{10.0}],[End{9.0}]}", scenario.toString());
	}

	@Test
	public void shouldDecideNextNodeRandomly() {
		int NODE_0_ID = 0;
		int NODE_1_ID = 1;
		int NODE_2_ID = 2;
		// Arrange
		Link[] node0Links = { new Link(1, 1) };
		Link[] node1Links = { new Link(0, 0.25), new Link(2, 0.75) };
		Link[] node2Links = {};
		Node[] nodes = { new Node(0, "Start", COUNTER_START_VALUE, !Node.END_NODE, node0Links),
				new Node(1, "Node1", 0, !Node.END_NODE, node1Links), new Node(2, "End", 0, Node.END_NODE, node2Links) };

		// Path is 0 -> 1 -> 0 -> 1 -> 2
		when(mockGenerator.generate()).thenReturn(0.5).thenReturn(0.1).thenReturn(0.5).thenReturn(0.26);

		Scenario scenario = new Scenario(nodes, 0, mockGenerator, stop, ruleNext);

		// Act && Assert
		assertEquals(COUNTER_START_VALUE, scenario.getNodes()[NODE_0_ID].getCounter(), DELTA);

		scenario.advance(); // To node 1
		assertEquals(COUNTER_START_VALUE - 1, scenario.getNodes()[NODE_1_ID].getCounter(), DELTA);
		assertEquals(NODE_1_ID, scenario.getCurrentNode().getId());

		scenario.advance(); // To node 0
		assertEquals(COUNTER_START_VALUE - 2, scenario.getNodes()[NODE_0_ID].getCounter(), DELTA);
		assertEquals(NODE_0_ID, scenario.getCurrentNode().getId());

		scenario.advance(); // To node 1
		assertEquals(COUNTER_START_VALUE - 3, scenario.getNodes()[NODE_1_ID].getCounter(), DELTA);
		assertEquals(NODE_1_ID, scenario.getCurrentNode().getId());

		scenario.advance(); // To node 2
		assertEquals(COUNTER_START_VALUE - 4, scenario.getNodes()[NODE_2_ID].getCounter(), DELTA);
		assertEquals(NODE_2_ID, scenario.getCurrentNode().getId());

		verify(mockGenerator, times(4)).generate();
		assertEquals("{[Start{8.0}],[Node1{7.0}],[End{6.0}]}", scenario.toString());

	}

	@Test
	public void shouldRunScenario() {

		// Arrange
		Link[] node0Links = { new Link(1, 1) };
		Link[] node1Links = { new Link(0, 0.25), new Link(2, 0.75) };
		Link[] node2Links = {};
		Node[] nodes = { new Node(0, "Start", COUNTER_START_VALUE, !Node.END_NODE, node0Links),
				new Node(1, "Node1", Node.UNDEFINED_COUNTER, !Node.END_NODE, node1Links),
				new Node(2, "End", Node.UNDEFINED_COUNTER, Node.END_NODE, node2Links) };

		// Path is 0 -> 1 -> 0 -> 1 -> 2
		when(mockGenerator.generate()).thenReturn(0.5).thenReturn(0.1).thenReturn(0.5).thenReturn(0.26);

		Scenario scenario = new Scenario(nodes, 0, mockGenerator, stop, ruleNext);
		// Act
		scenario.run();

		// Assert
		assertEquals("{[Start{8.0}],[Node1{7.0}],[End{6.0}]}", scenario.toString());

	}

}
