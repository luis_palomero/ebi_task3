package com.luis.ebitests.graph.node;

import static org.junit.Assert.*;

import org.junit.Test;

import com.luis.ebitests.graph.node.EndOrNoCounterStopCriteria;
import com.luis.ebitests.graph.node.Link;
import com.luis.ebitests.graph.node.Node;

public class EndOrNoCounterStopCriteriaTest {

	@Test
	public void shouldStopWhenNodeIsLast() {
		// Arrange
		Link[] links = { new Link(1, 1) };
		Node node = new Node(1, "Node1", 1, Node.END_NODE, links);
		EndOrNoCounterStopCriteria criteria = new EndOrNoCounterStopCriteria();
		// Act
		boolean mustStop = criteria.checkStop(node);

		// Assert
		assertTrue(mustStop);

	}

	@Test
	public void shouldStopWhenNodeHasNotCount() {
		// Arrange
		Link[] links = { new Link(1, 1) };
		Node node = new Node(1, "Node1", 0, !Node.END_NODE, links);
		EndOrNoCounterStopCriteria criteria = new EndOrNoCounterStopCriteria();
		// Act
		boolean mustStop = criteria.checkStop(node);

		// Assert
		assertTrue(mustStop);

	}

	@Test
	public void shouldStopWhenNodeCountAndNotLast() {
		// Arrange
		Link[] links = { new Link(1, 1) };
		Node node = new Node(1, "Node1", 1, !Node.END_NODE, links);
		EndOrNoCounterStopCriteria criteria = new EndOrNoCounterStopCriteria();
		// Act
		boolean mustStop = criteria.checkStop(node);

		// Assert
		assertFalse(mustStop);

	}

}
