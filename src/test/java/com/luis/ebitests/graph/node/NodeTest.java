package com.luis.ebitests.graph.node;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Test;

import com.luis.ebitests.graph.node.Link;
import com.luis.ebitests.graph.node.Node;

public class NodeTest {

	@Test(expected = IllegalStateException.class)
	public void SumOfProbLinksMustBe1AndWillThrowExceptionIfNot() {
		// Arrange
		Link links[] = { new Link(1, 0.5) };
		// Act
		Node node = new Node(1, "Node1", 1, !Node.END_NODE, links);
	}

	@Test
	public void NodeCanHave0LinksIfIsEnd(){
		// Arrange
		Link links[] = {};
		// Act
		Node node = new Node(1, "Node1", 1, Node.END_NODE, links);

		// Assert
		assertEquals(links.length, node.getLinks().length);
	}
	

	@Test(expected = IllegalStateException.class)
	public void NodeCanNotHave0LinksIfIsNotEnd(){
		// Arrange
		Link links[] = {};
		// Act
		Node node = new Node(1, "Node1", 1, !Node.END_NODE, links);
	}
	
	@Test
	public void WithNodesSumOrProbLinksMustBe1() {
		// Arrange
		Link links[] = { new Link(1, 0.5), new Link(1, 0.3), new Link(1, 0.2), };
		// Act
		Node node = new Node(1, "Node1", 1, !Node.END_NODE, links);

		// Assert
		assertEquals(links.length, node.getLinks().length);
	}

	@Test
	public void ShouldRetrieveUniqueNodeIfNoOtherAvailable() {
		// Arrange
		Link links[] = { new Link(2, 1) };
		// Act
		Node node = new Node(1, "Node1", 1, !Node.END_NODE, links);
		int nextId = node.getIdNext(0.5);
		// Assert
		assertEquals(links[0].getId(), links[nextId].getId());
	}

	@Test
	public void ShouldRetrieveNextNodeUsingProbability() {
		// Arrange
		Link links[] = { new Link(2, 0.3), new Link(3, 0.7), };
		// Act
		Node node = new Node(1, "Node1", 1, !Node.END_NODE, links);
		int nextId = node.getIdNext(0.5);
		// Assert
		assertEquals(links[1].getId(), links[nextId].getId());
	}
	
	@Test
	public void ShouldRetrieveNextNodeUsingProbabilityAtEdge() {
		// Arrange
		int NODE_2_ID = 0;
		int NODE_3_ID = 1;
		Link links[] = { new Link(2, 0.3), new Link(3, 0.7), };
		// Act
		Node node = new Node(1, "Node1", 1, !Node.END_NODE, links);

		int nextIdNode2 = node.getIdNext(0.0);
		int nextIdNode2b = node.getIdNext(0.29);
		int nextIdNode2c = node.getIdNext(0.30);
		int nextIdNode3 = node.getIdNext(0.31);
		int nextIdNode3b = node.getIdNext(1);

		// Assert
		assertEquals(links[NODE_2_ID].getId(), links[nextIdNode2].getId());
		assertEquals(links[NODE_2_ID].getId(), links[nextIdNode2b].getId());
		assertEquals(links[NODE_2_ID].getId(), links[nextIdNode2c].getId());
		assertEquals(links[NODE_3_ID].getId(), links[nextIdNode3].getId());
		assertEquals(links[NODE_3_ID].getId(), links[nextIdNode3b].getId());
	}

	@Test
	public void ShouldReturnValidString(){
		//Arrange
		Link links[] = { new Link(2, 0.3), new Link(3, 0.7), };
		Node node = new Node(1, "Node1", 1, !Node.END_NODE, links);
		
		// Act
		String output = node.toString();
		
		//Assert
		assertEquals("[Node1{1.0}]", output);
	}
	
	@Test
	public void ShouldReplaceUndefinedByScpecialChar(){
		//Arrange
		Link links[] = { new Link(2, 0.3), new Link(3, 0.7), };
		Node node = new Node(1, "Node1", Node.UNDEFINED_COUNTER, !Node.END_NODE, links);
		
		// Act
		String output = node.toString();
		
		//Assert
		assertEquals("[Node1{?}]", output);
	}

}
